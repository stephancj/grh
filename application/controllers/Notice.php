<?php
 
class Notice extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Notice_model');
    } 

    /*
     * Listing of notice
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('notice/index?');
        $config['total_rows'] = $this->Notice_model->get_all_notice_count();
        $this->pagination->initialize($config);

        $data['notice'] = $this->Notice_model->get_all_notice($params);
        
        $data['_view'] = 'notice/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new notice
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'date' => date('Y-m-d'),
				'titre' => $this->input->post('titre'),
				'url_fichier' => $this->input->post('url_fichier'),
				'description' => $this->input->post('description'),
            );
            
            $notice_id = $this->Notice_model->add_notice($params);
            redirect('notice/index');
        }
        else
        {            
            $data['_view'] = 'notice/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a notice
     */
    function edit($id)
    {   
        // check if the notice exists before trying to edit it
        $data['notice'] = $this->Notice_model->get_notice($id);
        
        if(isset($data['notice']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'titre' => $this->input->post('titre'),
					'url_fichier' => $this->input->post('url_fichier'),
					'description' => $this->input->post('description'),
                );

                $this->Notice_model->update_notice($id,$params);            
                redirect('notice/index');
            }
            else
            {
                $data['_view'] = 'notice/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The notice you are trying to edit does not exist.');
    } 

    /*
     * Deleting notice
     */
    function remove($id)
    {
        $notice = $this->Notice_model->get_notice($id);

        // check if the notice exists before trying to delete it
        if(isset($notice['id']))
        {
            $this->Notice_model->delete_notice($id);
            redirect('notice/index');
        }
        else
            show_error('The notice you are trying to delete does not exist.');
    }
    
}

