<?php

class Conge extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Conge_model');
    } 

    /*
     * Listing of conge
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('conge/index?');
        $config['total_rows'] = $this->Conge_model->get_all_conge_count();
        $this->pagination->initialize($config);

        $data['conge'] = $this->Conge_model->get_all_conge($params);
        
        $data['_view'] = 'conge/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Listing of conge per user
     */
    function user()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('conge/user?');
        $config['total_rows'] = $this->Conge_model->get_all_conge_count();
        $this->pagination->initialize($config);

        $data['conge'] = $this->Conge_model->get_all_conge($params);
        
        $data['_view'] = 'conge/user';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new conge
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'type_conge_id' => $this->input->post('type_conge_id'),
				'date_debut' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_debut')))),
				'date_fin' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_fin')))),
				'duree' => $this->input->post('duree'),
				'raison' => $this->input->post('raison'),
				'date_application' => date('Y-m-d'),
				'statut' => 'En attente',
				'employe_matricule' => 0,
            );
            
            $conge_id = $this->Conge_model->add_conge($params);
            redirect('conge/index');
        }
        else
        {
			$this->load->model('Type_conge_model');
			$data['all_type_conge'] = $this->Type_conge_model->get_all_type_conge();
            
            $data['_view'] = 'conge/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a conge
     */
    function edit($id)
    {   
        // check if the conge exists before trying to edit it
        $data['conge'] = $this->Conge_model->get_conge($id);
        
        if(isset($data['conge']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'type_conge_id' => $this->input->post('type_conge_id'),
					'date_debut' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_debut')))),
			    	'date_fin' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_fin')))),
					'duree' => $this->input->post('duree'),
					'raison' => $this->input->post('raison'),
					'statut' => $this->input->post('statut'),
					'employe_matricule' => $this->input->post('employe_matricule'),
                );

                $this->Conge_model->update_conge($id,$params);            
                redirect('conge/index');
            }
            else
            {
				$this->load->model('Type_conge_model');
				$data['all_type_conge'] = $this->Type_conge_model->get_all_type_conge();

                $data['_view'] = 'conge/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The conge you are trying to edit does not exist.');
    } 

    /*
     * Approving a conge
     */
    function approve($id)
    {   
        // check if the conge exists before trying to edit it
        $conge = $this->Conge_model->get_conge($id);
        
        if(isset($conge['id']))
        {     
                $params = array(
					'statut' => 'approuvé',
                );

                $this->Conge_model->update_conge($id,$params);            
                redirect('conge/index');
        }
        else
            show_error('The conge you are trying to edit does not exist.');
    } 

    /*
     * refusing a conge
     */
    function refuse($id)
    {   
        // check if the conge exists before trying to edit it
        $conge = $this->Conge_model->get_conge($id);
        
        if(isset($conge['id']))
        {     
                $params = array(
					'statut' => 'décliné',
                );

                $this->Conge_model->update_conge($id,$params);            
                redirect('conge/index');
        }
        else
            show_error('The conge you are trying to edit does not exist.');
    } 

    /*
     * Deleting conge
     */
    function remove($id)
    {
        $conge = $this->Conge_model->get_conge($id);

        // check if the conge exists before trying to delete it
        if(isset($conge['id']))
        {
            $this->Conge_model->delete_conge($id);
            redirect('conge/index');
        }
        else
            show_error('The conge you are trying to delete does not exist.');
    }
    
}
