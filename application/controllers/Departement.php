<?php
 
class Departement extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Departement_model');
    } 

    /*
     * Listing of departement
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('departement/index?');
        $config['total_rows'] = $this->Departement_model->get_all_departement_count();
        $this->pagination->initialize($config);

        $data['departement'] = $this->Departement_model->get_all_departement($params);
        
        $data['_view'] = 'departement/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new departement
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nom' => $this->input->post('nom'),
            );
            
            $departement_id = $this->Departement_model->add_departement($params);
            redirect('departement/index');
        }
        else
        {            
            $data['_view'] = 'departement/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a departement
     */
    function edit($id)
    {   
        // check if the departement exists before trying to edit it
        $data['departement'] = $this->Departement_model->get_departement($id);
        
        if(isset($data['departement']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nom' => $this->input->post('nom'),
                );

                $this->Departement_model->update_departement($id,$params);            
                redirect('departement/index');
            }
            else
            {
                $data['_view'] = 'departement/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The departement you are trying to edit does not exist.');
    } 

    /*
     * Deleting departement
     */
    function remove($id)
    {
        $departement = $this->Departement_model->get_departement($id);

        // check if the departement exists before trying to delete it
        if(isset($departement['id']))
        {
            $this->Departement_model->delete_departement($id);
            redirect('departement/index');
        }
        else
            show_error('The departement you are trying to delete does not exist.');
    }
    
}
