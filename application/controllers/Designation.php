<?php

 
class Designation extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Designation_model');
    } 

    /*
     * Listing of designation
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('designation/index?');
        $config['total_rows'] = $this->Designation_model->get_all_designation_count();
        $this->pagination->initialize($config);

        $data['designation'] = $this->Designation_model->get_all_designation($params);
        
        $data['_view'] = 'designation/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new designation
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nom' => $this->input->post('nom'),
            );
            
            $designation_id = $this->Designation_model->add_designation($params);
            redirect('designation/index');
        }
        else
        {            
            $data['_view'] = 'designation/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a designation
     */
    function edit($id)
    {   
        // check if the designation exists before trying to edit it
        $data['designation'] = $this->Designation_model->get_designation($id);
        
        if(isset($data['designation']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nom' => $this->input->post('nom'),
                );

                $this->Designation_model->update_designation($id,$params);            
                redirect('designation/index');
            }
            else
            {
                $data['_view'] = 'designation/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The designation you are trying to edit does not exist.');
    } 

    /*
     * Deleting designation
     */
    function remove($id)
    {
        $designation = $this->Designation_model->get_designation($id);

        // check if the designation exists before trying to delete it
        if(isset($designation['id']))
        {
            $this->Designation_model->delete_designation($id);
            redirect('designation/index');
        }
        else
            show_error('The designation you are trying to delete does not exist.');
    }
    
}
