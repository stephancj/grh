<?php
 
class To-do-list extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('To-do-list_model');
    } 

    /*
     * Listing of to-do-list
     */
    function index()
    {
        $data['to-do-list'] = $this->To-do-list_model->get_all_to-do-list();
        
        $data['_view'] = 'to-do-list/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new to-do-list
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'statut' => $this->input->post('statut'),
				'tache' => $this->input->post('tache'),
				'date_creation' => $this->input->post('date_creation'),
				'employe_matricule' => $this->input->post('employe_matricule'),
            );
            
            $to-do-list_id = $this->To-do-list_model->add_to-do-list($params);
            redirect('to-do-list/index');
        }
        else
        {            
            $data['_view'] = 'to-do-list/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a to-do-list
     */
    function edit($id)
    {   
        // check if the to-do-list exists before trying to edit it
        $data['to-do-list'] = $this->To-do-list_model->get_to-do-list($id);
        
        if(isset($data['to-do-list']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'statut' => $this->input->post('statut'),
					'tache' => $this->input->post('tache'),
					'date_creation' => $this->input->post('date_creation'),
					'employe_matricule' => $this->input->post('employe_matricule'),
                );

                $this->To-do-list_model->update_to-do-list($id,$params);            
                redirect('to-do-list/index');
            }
            else
            {
                $data['_view'] = 'to-do-list/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The to-do-list you are trying to edit does not exist.');
    } 

    /*
     * Deleting to-do-list
     */
    function remove($id)
    {
        $to-do-list = $this->To-do-list_model->get_to-do-list($id);

        // check if the to-do-list exists before trying to delete it
        if(isset($to-do-list['id']))
        {
            $this->To-do-list_model->delete_to-do-list($id);
            redirect('to-do-list/index');
        }
        else
            show_error('The to-do-list you are trying to delete does not exist.');
    }
    
}
