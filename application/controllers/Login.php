<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('login_model');
        $this->load->model('notice_model');
  
    }
    
	public function index()
	{
		#Redirect to Admin dashboard after authentication
        if ($this->session->userdata('user_login_access') == 1)
            redirect(base_url() . 'dashboard');
            $data=array();
            #$data['settingsvalue'] = $this->dashboard_model->GetSettingsValue();
			$this->load->view('login');
	}
	public function Login_Auth(){	
	$response = array();
    //Recieving post input of email, mot_de_passe from request
    $email = $this->input->post('email');
    $password = ($this->input->post('password'));
	$remember = $this->input->post('remember');
	#Login input validation\
	$this->load->library('form_validation');
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	$this->form_validation->set_rules('email', 'User Email', 'trim|xss_clean|required|min_length[7]');
	$this->form_validation->set_rules('password', 'Mot de passe', 'trim|xss_clean|required|min_length[4]');
	
	if($this->form_validation->run() == FALSE){
		$this->session->set_flashdata('feedback','UserEmail or mot_de_passe is Invalid');
		redirect(base_url() . 'login', 'refresh');		
	}
	else{
        //Validating login
        $login_status = $this->validate_login($email, $password);
        $response['login_status'] = $login_status;
        if ($login_status == 'success') {
        	if($remember){
        		setcookie('email',$email,time() + (86400 * 30));
        		setcookie('password',$this->input->post('password'),time() + (86400 * 30));
        		redirect(base_url() . 'login', 'refresh');
        		
        	} else {
        		if(isset($_COOKIE['email']))
        		{
        			setcookie('email',' ');
        		}
        		if(isset($_COOKIE['password']))
        		{
        			setcookie('password',' ');
        		}        		
        		redirect(base_url() . 'login', 'refresh');
        	}
        
        }
		else{
			$this->session->set_flashdata('feedback','UserEmail or mot_de_passe is Invalid');
			redirect(base_url() . 'login', 'refresh');
		}
	}
	}
    //Validating login from request
    function validate_login($email = '', $password = '') {
        $credential = array('email' => $email, 'mot_de_passe' => $password);


        $query = $this->login_model->getUserForLogin($credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->session->set_userdata('user_login_access', '1');
            $this->session->set_userdata('user_login_id', $row->matricule);
			$this->session->set_userdata('name', $row->nom);
			$this->session->set_userdata('firstname', $row->prenom);
            $this->session->set_userdata('email', $row->email);
            $this->session->set_userdata('user_image', $row->photo);
			$this->session->set_userdata('user_type', $row->role);
			// $this->session->set_userdata('user_des', $row->designation);
			// $this->session->set_userdata('user_dep', $row->departement);
            return 'success';
        }
	}
    /*Logout method*/
    function logout() {
        $this->session->sess_destroy();
        $this->session->set_flashdata('feedback', 'logged_out');
        redirect(base_url(), 'refresh');
    }
	
}