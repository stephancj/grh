<?php

 
class Type_conge extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Type_conge_model');
    } 

    /*
     * Listing of type_conge
     */
    function index()
    {
        $data['type_conge'] = $this->Type_conge_model->get_all_type_conge();
        
        $data['_view'] = 'type_conge/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new type_conge
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nom' => $this->input->post('nom'),
				'nb_jour' => $this->input->post('nb_jour'),
            );
            
            $type_conge_id = $this->Type_conge_model->add_type_conge($params);
            redirect('type_conge/index');
        }
        else
        {            
            $data['_view'] = 'type_conge/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a type_conge
     */
    function edit($id)
    {   
        // check if the type_conge exists before trying to edit it
        $data['type_conge'] = $this->Type_conge_model->get_type_conge($id);
        
        if(isset($data['type_conge']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nom' => $this->input->post('nom'),
					'nb_jour' => $this->input->post('nb_jour'),
                );

                $this->Type_conge_model->update_type_conge($id,$params);            
                redirect('type_conge/index');
            }
            else
            {
                $data['_view'] = 'type_conge/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The type_conge you are trying to edit does not exist.');
    } 

    /*
     * Deleting type_conge
     */
    function remove($id)
    {
        $type_conge = $this->Type_conge_model->get_type_conge($id);

        // check if the type_conge exists before trying to delete it
        if(isset($type_conge['id']))
        {
            $this->Type_conge_model->delete_type_conge($id);
            redirect('type_conge/index');
        }
        else
            show_error('The type_conge you are trying to delete does not exist.');
    }
    
}
