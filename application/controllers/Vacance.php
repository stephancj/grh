<?php
 
class Vacance extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Vacance_model');
    } 

    /*
     * Listing of vacance
     */
    function index()
    {
        $data['vacance'] = $this->Vacance_model->get_all_vacance();
        
        $data['_view'] = 'vacance/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new vacance
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nom' => $this->input->post('nom'),
				'date_debut' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_debut')))),
				'date_fin' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_fin')))),
				'annee' => $this->input->post('annee'),
            );
            
            $vacance_id = $this->Vacance_model->add_vacance($params);
            redirect('vacance/index');
        }
        else
        {            
            $data['_view'] = 'vacance/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a vacance
     */
    function edit($id)
    {   
        // check if the vacance exists before trying to edit it
        $data['vacance'] = $this->Vacance_model->get_vacance($id);
        
        if(isset($data['vacance']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nom' => $this->input->post('nom'),
					'date_debut' => $this->input->post('date_debut'),
					'date_fin' => $this->input->post('date_fin'),
					'annee' => $this->input->post('annee'),
                );

                $this->Vacance_model->update_vacance($id,$params);            
                redirect('vacance/index');
            }
            else
            {
                $data['_view'] = 'vacance/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The vacance you are trying to edit does not exist.');
    } 

    /*
     * Deleting vacance
     */
    function remove($id)
    {
        $vacance = $this->Vacance_model->get_vacance($id);

        // check if the vacance exists before trying to delete it
        if(isset($vacance['id']))
        {
            $this->Vacance_model->delete_vacance($id);
            redirect('vacance/index');
        }
        else
            show_error('The vacance you are trying to delete does not exist.');
    }
    
}
