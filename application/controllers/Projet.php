<?php

 
class Projet extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Projet_model');
    } 

    /*
     * Listing of projet
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('projet/index?');
        $config['total_rows'] = $this->Projet_model->get_all_projet_count();
        $this->pagination->initialize($config);

        $data['projet'] = $this->Projet_model->get_all_projet($params);
        
        $data['_view'] = 'projet/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new projet
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nom' => $this->input->post('nom'),
				'date_debut' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_debut')))),
				'date_fin' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_fin')))),
				'statut' => $this->input->post('statut'),
				'description' => $this->input->post('description'),
				'resume' => $this->input->post('resume'),
            );
            
            $projet_id = $this->Projet_model->add_projet($params);
            redirect('projet/index');
        }
        else
        {            
            $data['_view'] = 'projet/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a projet
     */
    function edit($id)
    {   
        // check if the projet exists before trying to edit it
        $data['projet'] = $this->Projet_model->get_projet($id);
        
        if(isset($data['projet']['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nom' => $this->input->post('nom'),
					'date_debut' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_debut')))),
					'date_fin' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date_fin')))),
					'statut' => $this->input->post('statut'),
					'description' => $this->input->post('description'),
					'resume' => $this->input->post('resume'),
                );

                $this->Projet_model->update_projet($id,$params);            
                redirect('projet/index');
            }
            else
            {
                $data['_view'] = 'projet/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The projet you are trying to edit does not exist.');
    } 

    /*
     * Deleting projet
     */
    function remove($id)
    {
        $projet = $this->Projet_model->get_projet($id);

        // check if the projet exists before trying to delete it
        if(isset($projet['id']))
        {
            $this->Projet_model->delete_projet($id);
            redirect('projet/index');
        }
        else
            show_error('The projet you are trying to delete does not exist.');
    }
    
}
