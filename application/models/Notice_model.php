<?php
 
class Notice_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get notice by id
     */
    function get_notice($id)
    {
        return $this->db->get_where('notice',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all notice count
     */
    function get_all_notice_count()
    {
        $this->db->from('notice');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all notice
     */
    function get_all_notice($params = array())
    {
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('notice')->result_array();
    }
        
    /*
     * function to add new notice
     */
    function add_notice($params)
    {
        $this->db->insert('notice',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update notice
     */
    function update_notice($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('notice',$params);
    }
    
    /*
     * function to delete notice
     */
    function delete_notice($id)
    {
        return $this->db->delete('notice',array('id'=>$id));
    }
}
