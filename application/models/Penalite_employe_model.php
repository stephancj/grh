<?php
 
class Penalite_employe_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get penalite_employe by id
     */
    function get_penalite_employe($id)
    {
        return $this->db->get_where('penalite_employe',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all penalite_employe count
     */
    function get_all_penalite_employe_count()
    {
        $this->db->from('penalite_employe');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all penalite_employe
     */
    function get_all_penalite_employe($params = array())
    {
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('penalite_employe')->result_array();
    }
        
    /*
     * function to add new penalite_employe
     */
    function add_penalite_employe($params)
    {
        $this->db->insert('penalite_employe',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update penalite_employe
     */
    function update_penalite_employe($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('penalite_employe',$params);
    }
    
    /*
     * function to delete penalite_employe
     */
    function delete_penalite_employe($id)
    {
        return $this->db->delete('penalite_employe',array('id'=>$id));
    }
}
