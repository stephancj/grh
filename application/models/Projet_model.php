<?php
 
class Projet_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get projet by id
     */
    function get_projet($id)
    {
        return $this->db->get_where('projet',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all projet count
     */
    function get_all_projet_count()
    {
        $this->db->from('projet');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all projet
     */
    function get_all_projet($params = array())
    {
        $this->db->order_by('id', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('projet')->result_array();
    }
        
    /*
     * function to add new projet
     */
    function add_projet($params)
    {
        $this->db->insert('projet',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update projet
     */
    function update_projet($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('projet',$params);
    }
    
    /*
     * function to delete projet
     */
    function delete_projet($id)
    {
        return $this->db->delete('projet',array('id'=>$id));
    }
}
