<?php
 
class Employe_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get employe by matricule
     */
    function get_employe($matricule)
    {
        return $this->db->get_where('employe',array('matricule'=>$matricule))->row_array();
    }
    
    /*
     * Get all employe count
     */
    function get_all_employe_count()
    {
        $this->db->from('employe');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all employe
     */
    function get_all_employe($params = array())
    {
        $this->db->order_by('matricule', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('employe')->result_array();
    }

    /*
     * Get to retired
     */
    function get_to_retired($params = array())
    {
        $this->db->where('date_anniv <=', date("Y-m-d", strtotime("-59 year", strtotime(date("Y-m-d")))));
        $this->db->order_by('date_anniv', 'asc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('employe')->result_array();
    }
        
    /*
     * function to add new employe
     */
    function add_employe($params)
    {
        $this->db->insert('employe',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update employe
     */
    function update_employe($matricule,$params)
    {
        $this->db->where('matricule',$matricule);
        return $this->db->update('employe',$params);
    }
    
    /*
     * function to delete employe
     */
    function delete_employe($matricule)
    {
        return $this->db->delete('employe',array('matricule'=>$matricule));
    }
}
