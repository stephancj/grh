<?php
 
class Penalite_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get penalite by id
     */
    function get_penalite($id)
    {
        return $this->db->get_where('penalite',array('id'=>$id))->row_array();
    }
        
    /*
     * Get all penalite
     */
    function get_all_penalite()
    {
        $this->db->order_by('id', 'desc');
        return $this->db->get('penalite')->result_array();
    }
        
    /*
     * function to add new penalite
     */
    function add_penalite($params)
    {
        $this->db->insert('penalite',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update penalite
     */
    function update_penalite($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('penalite',$params);
    }
    
    /*
     * function to delete penalite
     */
    function delete_penalite($id)
    {
        return $this->db->delete('penalite',array('id'=>$id));
    }
}
