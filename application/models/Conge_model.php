<?php

class Conge_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get conge by id
     */
    function get_conge($id)
    {
        return $this->db->get_where('conge',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all conge count
     */
    function get_all_conge_count()
    {
        $this->db->from('conge');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all conge
     */
    function get_all_conge($params = array())
    {
        // $this->db->order_by('date_application', 'desc');
        $query = $this->db->query('SELECT conge.id id, employe.nom nom, employe.prenom prenom, type_conge.nom type_conge, conge.date_debut, conge.date_fin, conge.duree, conge.raison, conge.date_application, conge.statut 
            FROM conge JOIN employe ON conge.employe_matricule=employe.matricule 
            JOIN type_conge ON conge.type_conge_id=type_conge.id 
            ORDER BY conge.date_application DESC');
        if($query->num_rows()>0){
            $result = $query->result_array();
        }
        return $result;
        // if(isset($params) && !empty($params))
        // {
        //     $this->db->limit($params['limit'], $params['offset']);
        // }
        // return $this->db->get('conge')->result_array();
    }
        
    /*
     * function to add new conge
     */
    function add_conge($params)
    {
        $this->db->insert('conge',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update conge
     */
    function update_conge($id,$params)
    {
        $this->db->where('id',$id);
        return $this->db->update('conge',$params);
    }
    
    /*
     * function to delete conge
     */
    function delete_conge($id)
    {
        return $this->db->delete('conge',array('id'=>$id));
    }
}
