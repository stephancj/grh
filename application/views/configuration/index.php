<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Configuration site</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>Logo Site</th>
						<th>Titre Site</th>
						<th>Copyright</th>
						<th>Contact</th>
						<th>Adresse</th>
						<th>Email Systeme</th>
						<th>Description</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($configuration as $c){ ?>
                    <tr>
						<td><?php echo $c['id']; ?></td>
						<td><?php echo $c['logo_site']; ?></td>
						<td><?php echo $c['titre_site']; ?></td>
						<td><?php echo $c['copyright']; ?></td>
						<td><?php echo $c['contact']; ?></td>
						<td><?php echo $c['adresse']; ?></td>
						<td><?php echo $c['email_systeme']; ?></td>
						<td><?php echo $c['description']; ?></td>
						<td>
                            <a href="<?php echo site_url('configuration/edit/'.$c['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('configuration/remove/'.$c['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
