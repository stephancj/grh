<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Employe Edit</h3>
            </div>
			<?php echo form_open('employe/edit/'.$employe['matricule']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="designation_id" class="control-label">Designation</label>
						<div class="form-group">
							<select name="designation_id" class="form-control">
								<option value="">Selectionnez designation</option>
								<?php 
								foreach($all_designation as $designation)
								{
									$selected = ($designation['id'] == $employe['designation_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$designation['id'].'" '.$selected.'>'.$designation['nom'].'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="departement_id" class="control-label">Departement</label>
						<div class="form-group">
							<select name="departement_id" class="form-control">
								<option value="">Selectionnez departement</option>
								<?php 
								foreach($all_departement as $departement)
								{
									$selected = ($departement['id'] == $employe['departement_id']) ? ' selected="selected"' : "";

									echo '<option value="'.$departement['id'].'" '.$selected.'>'.$departement['nom'].'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="nom" class="control-label">Nom</label>
						<div class="form-group">
							<input type="text" name="nom" value="<?php echo ($this->input->post('nom') ? $this->input->post('nom') : $employe['nom']); ?>" class="form-control" id="nom" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="prenom" class="control-label">Prenom</label>
						<div class="form-group">
							<input type="text" name="prenom" value="<?php echo ($this->input->post('prenom') ? $this->input->post('prenom') : $employe['prenom']); ?>" class="form-control" id="prenom" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="email" class="control-label">Email</label>
						<div class="form-group">
							<input type="email" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $employe['email']); ?>" class="form-control" id="email" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="mot_de_passe" class="control-label">Mot De Passe</label>
						<div class="form-group">
							<input type="text" name="mot_de_passe" value="<?php echo ($this->input->post('mot_de_passe') ? $this->input->post('mot_de_passe') : $employe['mot_de_passe']); ?>" class="form-control" id="mot_de_passe" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="role" class="control-label">Role</label>
						<div class="form-group">
							<select name="role" class="form-control">
								<option value="">Selectionnez Role</option>
								<?php 
								$roles = ['Employé',' Administrateur',' RH'];
								foreach($roles as $role)
								{
									$selected = ($role == $this->input->post('role')) ? ' selected="selected"' : "";

									echo '<option value="'.$role.'" '.$selected.'>'.$role.'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="adresse" class="control-label">Adresse</label>
						<div class="form-group">
							<input type="text" name="adresse" value="<?php echo ($this->input->post('adresse') ? $this->input->post('adresse') : $employe['adresse']); ?>" class="form-control" id="adresse" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="statut" class="control-label">Statut</label>
						<div class="form-group">
							<input type="text" name="statut" value="<?php echo ($this->input->post('statut') ? $this->input->post('statut') : $employe['statut']); ?>" class="form-control" id="statut" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="genre" class="control-label">Genre</label>
						<div class="form-group">
							<select name="genre" class="form-control">
								<option value="">Selectionnez genre</option>
								<?php 
								$genres = ['Homme', 'Femme'];
								foreach($genres as $genre)
								{
									$selected = ($genre == $this->input->post('genre')) ? ' selected="selected"' : "";

									echo '<option value="'.$genre.'" '.$selected.'>'.$genre.'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="tel" class="control-label">Tel</label>
						<div class="form-group">
							<input type="text" name="tel" value="<?php echo ($this->input->post('tel') ? $this->input->post('tel') : $employe['tel']); ?>" class="form-control" id="tel" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_anniv" class="control-label">Date Anniv</label>
						<div class="form-group">
							<input type="text" name="date_anniv" value="<?php echo ($this->input->post('date_anniv') ? $this->input->post('date_anniv') : $employe['date_anniv']); ?>" class="has-datepicker form-control" id="date_anniv" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_recrutement" class="control-label">Date Recrutement</label>
						<div class="form-group">
							<input type="text" name="date_recrutement" value="<?php echo ($this->input->post('date_recrutement') ? $this->input->post('date_recrutement') : $employe['date_recrutement']); ?>" class="has-datepicker form-control" id="date_recrutement" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_fin_contrat" class="control-label">Date Fin Contrat</label>
						<div class="form-group">
							<input type="text" name="date_fin_contrat" value="<?php echo ($this->input->post('date_fin_contrat') ? $this->input->post('date_fin_contrat') : $employe['date_fin_contrat']); ?>" class="has-datepicker form-control" id="date_fin_contrat" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="grade" class="control-label">Grade</label>
						<div class="form-group">
							<input type="text" name="grade" value="<?php echo $this->input->post('grade'); ?>" class="form-control" id="grade" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="indice" class="control-label">Indice</label>
						<div class="form-group">
							<input type="number" name="indice" value="<?php echo $this->input->post('indice'); ?>" class="form-control" id="indice" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="photo" class="control-label">Photo</label>
						<div class="form-group">
							<input type="text" name="photo" value="<?php echo ($this->input->post('photo') ? $this->input->post('photo') : $employe['photo']); ?>" class="form-control" id="photo" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="conge_restant" class="control-label">Conge Restant</label>
						<div class="form-group">
							<input type="text" name="conge_restant" value="<?php echo ($this->input->post('conge_restant') ? $this->input->post('conge_restant') : $employe['conge_restant']); ?>" class="form-control" id="conge_restant" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Enregistrer
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>