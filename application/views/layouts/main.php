<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>grh</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/font-awesome.min.css');?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Datetimepicker -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap-datetimepicker.min.css');?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/AdminLTE.min.css');?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo site_url('resources/css/_all-skins.min.css');?>">
    </head>
    
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">GRH</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">GRH</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          <?php if( $this->session->userdata('user_type')=='RH' ){ ?>
            <?php 
                
                $this->db->where('statut','en attente');
                $this->db->from("conge");
                $nb_demande_conge = $this->db->count_all_results();

                $this->db->where('date_anniv <=', date("Y-m-d", strtotime("-59 year", strtotime(date("Y-m-d")))));
                $this->db->from("employe");
                $nb_proche_retraite = $this->db->count_all_results();

                $nb_notif = $nb_demande_conge + $nb_proche_retraite
            ?>

          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?php echo($nb_notif) ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header"><?php echo($nb_notif) ?> notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="<?php echo site_url('employe/proche_retraite');?>">
                      <i class="fa fa-users text-aqua"></i> <?php echo($nb_proche_retraite) ?> employé(s) proche de la retraite
                    </a>
                  </li>
                  <?php if($nb_demande_conge > 0 ) {?>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> 
                      <?php echo($nb_demande_conge);?> demande(s) de conge
                    </a>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </li>
          <?php } ?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $this->session->userdata('user_image'); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('firstname').' '.$this->session->userdata('name'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $this->session->userdata('user_image'); ?>" class="img-circle" alt="User Image">

                <p>
                <?php echo $this->session->userdata('firstname').' '.$this->session->userdata('name'); ?> - <?php echo $this->session->userdata('user_type'); ?>
                  <small>test</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <!-- <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profil</a>
                </div> -->
                <div class="pull-right">
                  <a href="<?php echo base_url(); ?>login/logout" class="btn btn-default btn-flat">Se déconnecter</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php $this->session->userdata('user_image'); ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $this->session->userdata('firstname').' '.$this->session->userdata('name'); ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> En ligne</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->

                    <?php if($this->session->userdata('user_type')=='Administrateur') { ?>
                    <ul class="sidebar-menu">
                        <li class="header">MENU PRINCIPAL</li>
                        <li>
                            <a href="<?php echo site_url();?>">
                                <i class="fa fa-file"></i> <span>Actualité</span>
                            </a>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-automobile"></i> <span>Congé</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('conge/add');?>"><i class="fa fa-plus"></i> Demander</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('conge/index');?>"><i class="fa fa-list-ul"></i> Congés des employés</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('conge/user');?>"><i class="fa fa-list-ul"></i> Vos congés</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('type_conge/index');?>"><i class="fa fa-list-ul"></i> Type de congés</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-building"></i> <span>Departement</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('departement/add');?>"><i class="fa fa-plus"></i> Ajout</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('departement/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Designation</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('designation/add');?>"><i class="fa fa-plus"></i> Ajout</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('designation/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-address-book"></i> <span>Employé</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('employe/add');?>"><i class="fa fa-plus"></i> Nouvel employé</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('employe/index');?>"><i class="fa fa-list-ul"></i> Annuaire</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('employe/proche_retraite');?>"><i class="fa fa-list-ul"></i> Proche de la retraite</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-desktop"></i> <span>Notice</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('notice/add');?>"><i class="fa fa-plus"></i> Ajouter</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('notice/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-exclamation-circle"></i> <span>Penalité</span>
                            </a>
                            <ul class="treeview-menu">
								<li>
                                    <a href="<?php echo site_url('penalite/index');?>"><i class="fa fa-list-ul"></i> Type de pénalité</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('penalite_employe/index');?>"><i class="fa fa-list-ul"></i> Pénalité employé</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-tasks"></i> <span>Projet</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('projet/add');?>"><i class="fa fa-plus"></i> Ajout</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('projet/index');?>"><i class="fa fa-list-ul"></i> Liste projet</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('tache/index');?>"><i class="fa fa-list-ul"></i> Taches</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-calendar"></i> <span>Vacances & jour feriés</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('vacance/add');?>"><i class="fa fa-plus"></i> Ajouter</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('vacance/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
                        <li>
                            <a href="<?php echo site_url('configuration/index');?>">
                                <i class="fa fa-cogs"></i> <span>Configuration</span>
                            </a>
                        </li>
                    </ul>
                    <?php } elseif($this->session->userdata('user_type')=='RH') { ?>
                    <ul class="sidebar-menu">
                        <li class="header">MENU PRINCIPAL</li>
                        <li>
                            <a href="<?php echo site_url();?>">
                                <i class="fa fa-file"></i> <span>Actualité</span>
                            </a>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-automobile"></i> <span>Congé</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('conge/add');?>"><i class="fa fa-plus"></i> Demander</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('conge/index');?>"><i class="fa fa-list-ul"></i> Congés des employés</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('conge/user');?>"><i class="fa fa-list-ul"></i> Vos congés</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('type_conge/index');?>"><i class="fa fa-list-ul"></i> Type de congés</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-building"></i> <span>Departement</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('departement/add');?>"><i class="fa fa-plus"></i> Ajout</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('departement/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Designation</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('designation/add');?>"><i class="fa fa-plus"></i> Ajout</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('designation/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-address-book"></i> <span>Employé</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('employe/add');?>"><i class="fa fa-plus"></i> Nouvel employé</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('employe/index');?>"><i class="fa fa-list-ul"></i> Annuaire</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('employe/proche_retraite');?>"><i class="fa fa-list-ul"></i> Proche de la retraite</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-desktop"></i> <span>Notice</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('notice/add');?>"><i class="fa fa-plus"></i> Ajouter</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('notice/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-exclamation-circle"></i> <span>Penalité</span>
                            </a>
                            <ul class="treeview-menu">
								<li>
                                    <a href="<?php echo site_url('penalite/index');?>"><i class="fa fa-list-ul"></i> Type de pénalité</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('penalite_employe/index');?>"><i class="fa fa-list-ul"></i> Pénalité employé</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-tasks"></i> <span>Projet</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('projet/add');?>"><i class="fa fa-plus"></i> Ajout</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('projet/index');?>"><i class="fa fa-list-ul"></i> Liste projet</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('tache/index');?>"><i class="fa fa-list-ul"></i> Taches</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-calendar"></i> <span>Vacances & jour feriés</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('vacance/add');?>"><i class="fa fa-plus"></i> Ajouter</a>
                                </li>
								<li>
                                    <a href="<?php echo site_url('vacance/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
                    </ul>
                    <?php }  elseif($this->session->userdata('user_type')=='Employé') { ?>
                    <ul class="sidebar-menu">
                        <li class="header">MENU PRINCIPAL</li>
                        <li>
                            <a href="<?php echo site_url();?>">
                                <i class="fa fa-file"></i> <span>Actualité</span>
                            </a>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-automobile"></i> <span>Congé</span>
                            </a>
                            <ul class="treeview-menu">
								<li class="active">
                                    <a href="<?php echo site_url('conge/add');?>"><i class="fa fa-plus"></i> Demander</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('conge/user');?>"><i class="fa fa-list-ul"></i> Vos congés</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('type_conge/index');?>"><i class="fa fa-list-ul"></i> Type de congés</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-building"></i> <span>Departement</span>
                            </a>
                            <ul class="treeview-menu">
								<li>
                                    <a href="<?php echo site_url('departement/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Designation</span>
                            </a>
                            <ul class="treeview-menu">
								<li>
                                    <a href="<?php echo site_url('designation/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-address-book"></i> <span>Employé</span>
                            </a>
                            <ul class="treeview-menu">
								<li>
                                    <a href="<?php echo site_url('employe/index');?>"><i class="fa fa-list-ul"></i> Annuaire</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-exclamation-circle"></i> <span>Penalité</span>
                            </a>
                            <ul class="treeview-menu">
								<li>
                                    <a href="<?php echo site_url('penalite/index');?>"><i class="fa fa-list-ul"></i> Type de pénalité</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('penalite_employe/index');?>"><i class="fa fa-list-ul"></i> Pénalité employé</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-tasks"></i> <span>Projet</span>
                            </a>
                            <ul class="treeview-menu">
								<li>
                                    <a href="<?php echo site_url('projet/index');?>"><i class="fa fa-list-ul"></i> Liste projet</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('tache/index');?>"><i class="fa fa-list-ul"></i> Taches</a>
                                </li>
							</ul>
                        </li>
						<li>
                            <a href="#">
                                <i class="fa fa-calendar"></i> <span>Vacances & jour feriés</span>
                            </a>
                            <ul class="treeview-menu">
								<li>
                                    <a href="<?php echo site_url('vacance/index');?>"><i class="fa fa-list-ul"></i> Liste</a>
                                </li>
							</ul>
                        </li>
                    </ul>
                    <?php } ?>

                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                    <?php                    
                    if(isset($_view) && $_view)
                        $this->load->view($_view);
                    ?>                    
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Copyright <a href="">Min Int</a> 2021</strong>
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">

                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo site_url('resources/js/jquery-2.2.3.min.js');?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo site_url('resources/js/bootstrap.min.js');?>"></script>
        <!-- FastClick -->
        <script src="<?php echo site_url('resources/js/fastclick.js');?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo site_url('resources/js/app.min.js');?>"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo site_url('resources/js/demo.js');?>"></script>
        <!-- DatePicker -->
        <script src="<?php echo site_url('resources/js/moment.js');?>"></script>
        <script src="<?php echo site_url('resources/js/bootstrap-datetimepicker.min.js');?>"></script>
        <script src="<?php echo site_url('resources/js/global.js');?>"></script>
    </body>
</html>
