<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Liste Notice</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('notice/add'); ?>" class="btn btn-success btn-sm">Ajouter</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Date</th>
						<th>Titre</th>
						<th>Url Fichier</th>
						<th>Description</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($notice as $n){ ?>
                    <tr>
						<td><?php echo $n['date']; ?></td>
						<td><?php echo $n['titre']; ?></td>
						<td><?php echo $n['url_fichier']; ?></td>
						<td><?php echo $n['description']; ?></td>
						<td>
                            <a href="<?php echo site_url('notice/edit/'.$n['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Modifier</a> 
                            <a href="<?php echo site_url('notice/remove/'.$n['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Supprimer</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="pull-right">
                    <?php echo $this->pagination->create_links(); ?>                    
                </div>                
            </div>
        </div>
    </div>
</div>
