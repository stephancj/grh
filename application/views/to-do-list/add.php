<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">To-do-list Add</h3>
            </div>
            <?php echo form_open('to-do-list/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox" name="statut" value="1"  id="statut" />
							<label for="statut" class="control-label">Statut</label>
						</div>
					</div>
					<div class="col-md-6">
						<label for="tache" class="control-label">Tache</label>
						<div class="form-group">
							<input type="text" name="tache" value="<?php echo $this->input->post('tache'); ?>" class="form-control" id="tache" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="date_creation" class="control-label">Date Creation</label>
						<div class="form-group">
							<input type="text" name="date_creation" value="<?php echo $this->input->post('date_creation'); ?>" class="has-datepicker form-control" id="date_creation" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="employe_matricule" class="control-label">Employe Matricule</label>
						<div class="form-group">
							<input type="text" name="employe_matricule" value="<?php echo $this->input->post('employe_matricule'); ?>" class="form-control" id="employe_matricule" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>