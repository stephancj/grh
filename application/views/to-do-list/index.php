<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">To-do-list Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('to-do-list/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>ID</th>
						<th>Statut</th>
						<th>Tache</th>
						<th>Date Creation</th>
						<th>Employe Matricule</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($to-do-list as $t){ ?>
                    <tr>
						<td><?php echo $t['id']; ?></td>
						<td><?php echo $t['statut']; ?></td>
						<td><?php echo $t['tache']; ?></td>
						<td><?php echo $t['date_creation']; ?></td>
						<td><?php echo $t['employe_matricule']; ?></td>
						<td>
                            <a href="<?php echo site_url('to-do-list/edit/'.$t['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('to-do-list/remove/'.$t['id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
