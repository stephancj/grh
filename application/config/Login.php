<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Login_Model;

class Login extends Controller
{
    public function index()
    {
        helper(['form']);
        echo view('login');
    } 

    public function auth()
    {
        $session = session();
        $model = new Login_model();
        $email = $this->request->getVar('email');
        $mot_de_passe = $this->request->getVar('mot_de_passe');
        $data = $model->where('email', $email)->first();
        if($data){
            $pass = $data['mot_de_passe'];
            $verify_pass = password_verify($mot_de_passe, $pass);
            if($verify_pass){
                $ses_data = [
                    'matricule'       => $data['matricule'],
                    'nom'     => $data['nom'],
                    'email'    => $data['email'],
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/dashboard');
            }else{
                $session->setFlashdata('msg', 'Mot de passe incorrect');
                return redirect()->to('/login');
            }
        }else{
            $session->setFlashdata('msg', 'Email inexistant');
            return redirect()->to('/login');
        }
    }

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }
} 