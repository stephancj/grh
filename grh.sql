-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 09 août 2021 à 01:39
-- Version du serveur :  10.4.17-MariaDB
-- Version de PHP : 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `grh`
--

-- --------------------------------------------------------

--
-- Structure de la table `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `logo_site` varchar(255) DEFAULT NULL,
  `titre_site` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `copyright` varchar(128) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `email_systeme` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `configuration`
--

INSERT INTO `configuration` (`id`, `logo_site`, `titre_site`, `description`, `copyright`, `contact`, `adresse`, `email_systeme`) VALUES
(1, NULL, 'GRH', 'Ministere de l\'interieur - Gestion des Ressources Humaine', 'Mininstere Interieur 2021', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `conge`
--

CREATE TABLE `conge` (
  `id` int(11) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `duree` int(11) NOT NULL,
  `raison` varchar(255) DEFAULT NULL,
  `date_application` date NOT NULL,
  `statut` enum('approuvé','décliné','en attente') NOT NULL,
  `employe_matricule` int(11) NOT NULL,
  `type_conge_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `conge`
--

INSERT INTO `conge` (`id`, `date_debut`, `date_fin`, `duree`, `raison`, `date_application`, `statut`, `employe_matricule`, `type_conge_id`) VALUES
(1, '2021-07-05', '2021-07-09', 5, 'test', '2021-07-19', 'approuvé', 0, 0),
(2, '2021-07-20', '2021-07-22', 3, 'test 3', '2021-07-19', 'décliné', 0, 0),
(3, '2021-08-09', '2021-08-13', 4, 'test', '2021-08-09', 'en attente', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

CREATE TABLE `departement` (
  `id` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `departement`
--

INSERT INTO `departement` (`id`, `nom`) VALUES
(1, 'Informatique'),
(2, 'Communication');

-- --------------------------------------------------------

--
-- Structure de la table `designation`
--

CREATE TABLE `designation` (
  `id` int(11) NOT NULL,
  `nom` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `designation`
--

INSERT INTO `designation` (`id`, `nom`) VALUES
(1, 'Chef de departement'),
(2, 'Developpeur');

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE `employe` (
  `matricule` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `mot_de_passe` varchar(128) DEFAULT NULL,
  `role` enum('Employé','Administrateur','RH') NOT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `statut` enum('actif','inactif') DEFAULT NULL,
  `genre` enum('Homme','Femme') DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `date_anniv` date DEFAULT NULL,
  `date_recrutement` date DEFAULT NULL,
  `date_fin_contrat` date DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `departement_id` int(11) DEFAULT NULL,
  `conge_restant` int(11) DEFAULT NULL,
  `indice` int(11) DEFAULT NULL,
  `grade` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `employe`
--

INSERT INTO `employe` (`matricule`, `nom`, `prenom`, `email`, `mot_de_passe`, `role`, `adresse`, `statut`, `genre`, `tel`, `date_anniv`, `date_recrutement`, `date_fin_contrat`, `photo`, `designation_id`, `departement_id`, `conge_restant`, `indice`, `grade`) VALUES
(0, 'CHRISTIAN', 'Stéphan', 'christiantephanjosue@gmail.com', '151297', 'RH', '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 28, 0, ''),
(1111, 'Mario', 'Mario', 'mario@mario.com', 'mario', 'Employé', '', '', '', '', '1970-01-01', '2021-07-25', '1970-01-01', '', 2, 2, 28, NULL, 'test'),
(1512, 'Razafiniaina', 'Steve Jonathan', 'christiantevejonathan@gmail.com', '151297', 'Administrateur', '', '', '', '', '1970-01-01', '2021-07-19', '2027-12-31', '', 0, 0, 28, 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `titre` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url_fichier` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `notice`
--

INSERT INTO `notice` (`id`, `date`, `titre`, `description`, `url_fichier`) VALUES
(1, '2021-07-20', 'Inauguration XXXXXXXXXX', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum quas doloremque ea inventore. Laboriosam libero ullam distinctio incidunt rerum perferendis iure at officiis enim, tempora consectetur? Voluptas porro unde assumenda!', ''),
(2, '2021-07-26', 'Lorem Ipsum', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum quas doloremque ea inventore. Laboriosam libero ullam distinctio incidunt rerum perferendis iure at officiis enim, tempora consectetur? Voluptas porro unde assumenda!', '');

-- --------------------------------------------------------

--
-- Structure de la table `penalite`
--

CREATE TABLE `penalite` (
  `id` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `penalite`
--

INSERT INTO `penalite` (`id`, `nom`) VALUES
(1, 'Premier avertissement'),
(2, 'Dernier avertissement');

-- --------------------------------------------------------

--
-- Structure de la table `penalite_employe`
--

CREATE TABLE `penalite_employe` (
  `id` int(11) NOT NULL,
  `penalite_id` int(11) NOT NULL,
  `employe_matricule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

CREATE TABLE `projet` (
  `id` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `description` text DEFAULT NULL,
  `resume` text DEFAULT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `statut` enum('En cours','Terminé','A venir','Annulé') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `projet`
--

INSERT INTO `projet` (`id`, `nom`, `description`, `resume`, `date_debut`, `date_fin`, `statut`) VALUES
(1, 'Building', 'Test Test Test Test', 'Test', '1970-01-01', '1970-01-01', 'En cours'),
(2, 'Building', 'sdsd', 'sdsd', '2021-07-21', '2021-07-21', 'A venir'),
(3, 'Test', 'asdasd', 'asdasd', '2021-07-21', '2021-07-20', 'Annulé'),
(4, 'sdfsdfsd', 'sdfgsd', 'sdfsd', '2021-07-21', '2021-07-20', 'Terminé'),
(6, 'Informatique', 'asdd', 'sdsd', '2021-07-19', '2021-07-24', 'En cours');

-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

CREATE TABLE `tache` (
  `id` int(11) NOT NULL,
  `titre` varchar(128) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `description` text DEFAULT NULL,
  `statut` varchar(10) DEFAULT NULL,
  `projet_id` int(11) NOT NULL,
  `employe_matricule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tache`
--

INSERT INTO `tache` (`id`, `titre`, `date_debut`, `date_fin`, `description`, `statut`, `projet_id`, `employe_matricule`) VALUES
(1, 'test', '2021-07-19', '2021-07-20', 'werdfg', NULL, 6, 1512),
(2, 'test', '2021-07-19', '2021-07-20', 'dhgjf', NULL, 3, 1512),
(3, 'erzzer', '2021-07-19', '2021-07-20', 'rtsdf', NULL, 2, 1512),
(4, 'Test222', '2021-07-19', '2021-07-20', 'Test', NULL, 3, 1512),
(5, 'erzzer', '2021-07-20', '2021-07-21', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum quas doloremque ea inventore. Laboriosam libero ullam distinctio incidunt rerum perferendis iure at officiis enim, tempora consectetur? Voluptas porro unde assumenda!', 'Terminé', 2, 1512);

-- --------------------------------------------------------

--
-- Structure de la table `to-do-list`
--

CREATE TABLE `to-do-list` (
  `id` int(11) NOT NULL,
  `tache` varchar(128) NOT NULL,
  `statut` tinyint(1) NOT NULL DEFAULT 0,
  `date_creation` date DEFAULT NULL,
  `employe_matricule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `type_conge`
--

CREATE TABLE `type_conge` (
  `id` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `nb_jour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `type_conge`
--

INSERT INTO `type_conge` (`id`, `nom`, `nb_jour`) VALUES
(0, 'Paternité', 5);

-- --------------------------------------------------------

--
-- Structure de la table `vacance`
--

CREATE TABLE `vacance` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date DEFAULT NULL,
  `annee` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vacance`
--

INSERT INTO `vacance` (`id`, `nom`, `date_debut`, `date_fin`, `annee`) VALUES
(0, 'Jour de Noel', '2021-12-25', '2021-12-25', 2021);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `conge`
--
ALTER TABLE `conge`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`matricule`);

--
-- Index pour la table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `penalite`
--
ALTER TABLE `penalite`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `penalite_employe`
--
ALTER TABLE `penalite_employe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `projet`
--
ALTER TABLE `projet`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tache`
--
ALTER TABLE `tache`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `to-do-list`
--
ALTER TABLE `to-do-list`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type_conge`
--
ALTER TABLE `type_conge`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vacance`
--
ALTER TABLE `vacance`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `conge`
--
ALTER TABLE `conge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `departement`
--
ALTER TABLE `departement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `penalite`
--
ALTER TABLE `penalite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `projet`
--
ALTER TABLE `projet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `tache`
--
ALTER TABLE `tache`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
